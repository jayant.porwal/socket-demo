use crate::{models, websocket};
use anyhow::{bail, Result};
use log::{error, info};
use serde::{Deserialize, Serialize};
use std::net::TcpStream;
use std::sync::atomic::{AtomicBool, Ordering};
use tokio_tungstenite::tungstenite::Message;
use tokio_tungstenite::tungstenite::{
    handshake::client::Response, stream::MaybeTlsStream, WebSocket,
};
#[derive(Serialize, Deserialize, Debug, Clone)]
#[serde(untagged)]
pub enum SubscribeSymbols {
    ETHUSDT,
    BTCUSDT,
}
pub struct BinanceWebSockets {
    exchange: String,
    ws_url: String,
    subscribe_symbols: SubscribeSymbols,
}
impl BinanceWebSockets {
    pub fn new(ws_url: String, subscribe_symbols: SubscribeSymbols) -> BinanceWebSockets {
        BinanceWebSockets {
            exchange: "binance".to_string(),
            ws_url,
            subscribe_symbols,
        }
    }

    pub async fn run(&mut self) -> Result<()> {
        let keep_running = AtomicBool::new(true);
        if let Err(e) = self.event_loop(&keep_running).await {
            error!("Error: {}", e);
        }
        info!("[{}] Loop stopped running.", &self.exchange);
        Ok(())
    }

    async fn event_loop(&mut self, running: &AtomicBool) -> Result<()> {
        info!("Establishing connection...");
        let mut socket = match self.connect().await {
            Ok(socket_ok) => socket_ok,
            Err(error) => {
                bail!("error: {}", error)
            }
        };
        info!("Connected!");

        info!("Start event loop...");
        while running.load(Ordering::Relaxed) {
            let message = match socket.0.read() {
                Ok(msg) => msg,
                Err(err) => {
                    error!("Error: {}", err);
                    info!("[{}] Reconnecting WebSocket due to error.", &self.exchange);
                    socket = match self.connect().await {
                        Ok(socket) => socket,
                        Err(error) => {
                            bail!("error: {}", error)
                        }
                    };
                    continue;
                }
            };
            match message {
                Message::Text(msg) => {
                    if let Err(e) = self.handle_msg(&msg).await {
                        error!("Error on handling stream message: {}", e);
                        continue;
                    }
                }
                // We can ignore these message because tungstenite takes care of them for us.
                Message::Ping(_) | Message::Pong(_) | Message::Binary(_) => (),
                Message::Close(e) => {
                    error!("Disconnected {:?}", e);
                    continue;
                }
                Message::Frame(_) => (),
            }
        }
        socket.0.close(None)?;

        Ok(())
    }

    async fn handle_msg(&mut self, msg: &str) -> Result<()> {
        let value: serde_json::Value = serde_json::from_str(msg)?;
        if let Ok(events) = serde_json::from_value::<BinanceEvents>(value) {
            match events {
                BinanceEvents::KLineEvent(v) => {
                    self.handle_k_line_event(v).await?;
                    return Ok(());
                }
                _ => {
                    info!(
                        "Generic event conversion not yet implemented for: BinanceEvents::{:?}",
                        events
                    );
                    return Ok(());
                }
            };
        } else {
            error!("Unknown message {}", msg);
        }
        Ok(())
    }

    async fn handle_k_line_event(&self, data: models::KLineEvent) -> Result<()> {
        info!("[{}] {:?}", self.exchange, data.s);
        // add your own logic here
        Ok(())
    }

    async fn connect(&mut self) -> Result<(WebSocket<MaybeTlsStream<TcpStream>>, Response)> {
        if let Ok(con) = websocket::connect_wss(
            &self.exchange,
            &format!("{}/ethusdt@kline_1m", &self.ws_url),
        ) {
            return Ok(con);
        }
        bail!("Unable to connect.");
    }
}

#[derive(Deserialize, Debug)]
#[serde(untagged)]
enum BinanceEvents {
    KLineEvent(models::KLineEvent),
    Unknown,
}
