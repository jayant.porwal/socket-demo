use anyhow::{bail, Result};
use log::{error, info};
use std::net::TcpStream;
use tokio_tungstenite::tungstenite::{
    self, handshake::client::Response, stream::MaybeTlsStream, WebSocket,
};
use url::Url;

pub fn connect_wss(
    exchange: &str,
    websocket_url: &str,
) -> Result<(WebSocket<MaybeTlsStream<TcpStream>>, Response)> {
    let max_retry = 5;
    for i in 0..max_retry {
        info!("[{}] connecting to {} (try {})", exchange, websocket_url, i);
        let url = Url::parse(websocket_url)?;

        match tungstenite::connect(url) {
            Ok(answer) => {
                return Ok(answer);
            }
            Err(e) => error!("Error during handshake {}", e),
        }
    }
    bail!(format!("Max connection retry reached"));
}
