use serde::Deserialize;
#[derive(Debug, Deserialize)]
pub struct KLineEvent {
    pub e: String,
    pub E: u64,
    pub s: String,
    pub k: KLineData,
}

#[derive(Debug, Deserialize)]
pub struct KLineData {
    pub t: u64,
    pub T: u64,
    pub s: String,
    pub i: String,
    pub f: u64,
    pub L: u64,
    pub o: String,
    pub c: String,
    pub h: String,
    pub l: String,
    pub v: String,
    pub n: i32,
    pub x: bool,
    pub q: String,
    pub V: String,
    pub Q: String,
    pub B: String,
}
