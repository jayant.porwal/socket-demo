use binance_websocket::{BinanceWebSockets, SubscribeSymbols};
use log::info;
use log4rs;
mod binance_websocket;
mod models;
mod websocket;

#[tokio::main]
async fn main() {
    log4rs::init_file("logconfig.yaml", Default::default()).expect("Log config file not found.");
    info!("We now have nice logging!");

    let mut binance_ws = BinanceWebSockets::new(
        "wss://stream.binance.com:443/ws".to_owned(),
        SubscribeSymbols::ETHUSDT,
    );
    binance_ws.run().await.unwrap();
}
